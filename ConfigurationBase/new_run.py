import os
import fileinput
# import commands
import socket
import re
import time
import sys
import subprocess

v1 = "--outputdir "
v2 = " --output output.xml "
file_path = "/home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/"
global filepath_1
filepath_1 = file_path + "/TestResults/"
filepath_2 = file_path + "/TestCases/"
v3 = " pybot --variable BROWSER:firefox "
v4 = " pybot --variable BROWSER:chrome "
global var
var="2"

# pybot --variable BROWSER:chrome login.txt

def ExecuteTestCases(v3,path):
    # cmd=v4+v2+filepath_2+"login.txt"
    # os.system(cmd)
    cmd = v3+"--outputdir "+path+"/Login --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/web_login.txt"
    os.system(cmd)
    cmd = v3+"--outputdir "+path+"/Hospital_Page --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/add_hospital.txt"
    os.system(cmd)
    # cmd = v3 + v2 + filepath_2+"add_hospital.txt"
    # os.system(cmd)
    # cmd=v3+v2+filepath_2+"add_patch.txt"
    # os.system(cmd)
    cmd = v3+"--outputdir "+path+"/Patch_Page --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/add_patch.txt"
    os.system(cmd)
    # cmd=v3+v2+filepath_2+"add_assistant.txt"
    # os.system(cmd)
    cmd = v3+"--outputdir "+path+"/Assistant_Page --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/add_assistant.txt"
    os.system(cmd)
    # cmd=v3+v2+filepath_2+"login.txt"
    # os.system(cmd)
    #cmd = "sudo pybot --variable BROWSER:chrome --outputdir /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestResults_chrome/Assistant_Page --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/login.txt"
    #os.system(cmd)
    # cmd = v4 + v2 + filepath_2+"add_hospital.txt"
    # os.system(cmd)
    #cmd = "sudo pybot --variable BROWSER:chrome --outputdir /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestResults_chrome/Hospital_Page --output output.xml /home/webcardio/Projects/wbc-test-suite/webcardio_web_automation/TestCases/add_hospital.txt"
    #os.system(cmd)
    # cmd=v4+v2+filepath_2+"add_patch.txt"
    # os.system(cmd)
    # cmd=v4+v2+filepath_2+"add_assistant.txt"
    # os.system(cmd)


# cmd=v1+filepath_1+"Add_Hospital"+v2+v3+filepath_2+"add_hospital.txt"
# os.system(cmd)
# cmd=v1+filepath_1+"Add_Patch"+v2+v3+filepath_2+"add_patch.txt"
# os.system(cmd)
# cmd=v1+filepath_1+"Add_Assistant"+v2+v3+filepath_2+"add_assistant.txt"
# os.system(cmd)
# cmd = v1 + filepath_1 + "Login" + v2 + v4 + filepath_2 + "login.txt"
# os.system(cmd)
# cmd = v1 + filepath_1 + "Add_Hospital" + v2 + v4 + filepath_2 + "add_hospital.txt"
# os.system(cmd)
# cmd = v1 + filepath_1 + "Add_Patch" + v2 + v4 + filepath_2 + "add_patch.txt"
# os.system(cmd)
# cmd = v1 + filepath_1 + "Add_Assistant" + v2 + v4 + filepath_2 + "add_assistant.txt"
# os.system(cmd)

def CombineLogs(path):
    rebot_files = ""
    cmd = ""
    command = "find " +path+ " -iname *.xml | xargs ls -tr | tr '\n' ' '"
    rebot_files = subprocess.check_output(
    command, shell=True)
    cmd = "sudo rebot --name WebCardioAutomationReport --outputdir " +path+ " -o output.xml --removekeywords passed --tagstatexclude FN* --tagstatexclude SH* " + \
        rebot_files
    os.system(cmd)

def SendingMail():
    cmd4="/home/webcardio/Projects/wbc-test-suite/webcardio_web_automation"
    #cmd1="zip -r "+file_path+"/"+apk+".zip "+file_path1
    cmd5="zip -r Report.zip TestResults"
    cmd2 = "mutt -s " + '"' + "WEB_REPORT" + '"' + " -a " + cmd4 + "/Report.zip" + " -- webcardio.test2@gmail.com < /dev/null"
    os.chdir(cmd4)
    os.system(cmd5)
    os.system(cmd2)
    #cmd6="rm -r TestResults"
    #os.system(cmd6)

print var
if var == "2":
    #print "hello"
    #global v3
    #v3="sudo pybot --variable BROWSER:firefox "
    print var
    v3 = " pybot --variable BROWSER:firefox "
    path_firefox = "/home/webcardio/Projects/wbc-test-suite/WebCardio_Automation/TestResults/TestResults_firfox"
    ExecuteTestCases(v3,path_firefox)
    CombineLogs(path_firefox)
    global varia
    varia = "3"

if varia == "3":
    v3 = " pybot --variable BROWSER:chrome "
    path_chrome = "/home/webcardio/Projects/wbc-test-suite/WebCardio_Automation/TestResults/TestResults_chrome"
    ExecuteTestCases(v3, path_chrome)
    CombineLogs(path_chrome)

SendingMail()