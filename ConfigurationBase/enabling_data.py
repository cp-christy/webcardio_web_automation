#!/usr/bin/env python
import os
import fileinput
#import commands
import socket
import re
import time
import sys
import subprocess
import random
from random import randint


def disable_data():
    cmd = " adb -s ZY223NXHPB shell svc data disable"
    os.system(cmd)
    return 0

def enable_data():
    cmd = " adb -s ZY223NXHPB shell svc data enable"
    os.system(cmd)
    return 0

def patient_age():
    global age
    age = random.randint(1, 150)
    #age = randint(100, 999)/100.00
    return age

def patient_weight():
    global weight
    weight = random.randint(1, 250)
    #weight = round(random.uniform(1, 350), 2)
    return weight

def patient_height():
    global height
    height = random.randint(1, 250)
    #height = round(random.uniform(5, 250), 2)
    return height
