*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
#Library           fakename.py
#Library   ../../hm_glblib.py
#Library   ../../ConfigFrameWork.py
#Library    ../DeviceLibrary/WifiConnectivity_lib.py
#Library    ../Tools/relay.py
Library    ../Tools/testraillib.py
#Library    ../DeviceLibrary/sensor_tests.py
Variables   ../Tools/TestRail_ParamFile.py
#Variables   ../Environment_ParamFile.py


*** Variables ***
#${hosp_name}  name
${SERVER}         wbcdev01.webcardio.in/admin/
#${BROWSER}        Chrome
#${BROWSERS}       Chrome  firefox
${browserType}          firefox
${DELAY}          10
${VALID_USER}     admin@wbcdev01.webcardio.in
${VALID_PASSWORD}    RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwMykSzDiq3PJCFXW
${LOGIN URL}      http://${SERVER}
${WELCOME URL}    http://${SERVER}
${ERROR URL}      http://${SERVER}/error.html
${LOGIN}    //*[@id="app"]/div/div/div/div[1]/form/div[1]/input
${PASSWORD1}    //*[@id="app"]/div/div/div/div[1]/form/div[2]/input
${LOGIN_BTN}    //*[@id="app"]/div/div/div/div[2]
#${ADD_TAB}   //*[@id="app"]/div/div/div/div/section/div/div/div/button
${DELETE_TAB}    //*[@id="app"]/div/div/div/div/div/nav/div[2]
${LOGIN_FAILED}  /html/body/div[2]/div
${ADD_BTN}  //*[@id="app"]/div/div/div/div/section/div/div/div/button
${Hospital_Name}    //*[@id="name"]
#${Hospital_Name}  /html/body/div[2]/div/div[2]/section/form/div[1]/input
${Hospital_Phone}  //*[@id="phone"]
${Hospital_Email}  //*[@id="email"]
${Hospital_Address}  //*[@id="address"]
${Hospital_Pincode}  //*[@id="pin"]
${Add_Hospital_BTN}  //*[@id="add"]
${Cancel_Hospital_BTN}    //*[@id="cancel"]
${Hospital_Tab}   //*[@id="app"]/div/div/header/div/nav/a[1]
${Patches_Tab}  //*[@id="app"]/div/div/header/div/nav/a[2]
${Doctors_Tab}  //*[@id="app"]/div/div/header/div/nav/a[3]
${Asst_Tab}  //*[@id="app"]/div/div/header/div/nav/a[4]
${Tech_Tab}     //*[@id="app"]/div/div/header/div/nav/a[5]
${Logout_Btn}   //*[@id="app"]/div/div/header/div/button
${Hospital_Name_Error}    /html/body/div[2]/div/div[2]/section/form/div[1]/span[2]
${Hospital_Phone_Error}    /html/body/div[2]/div/div[2]/section/form/div[2]/span[2]
${Hospital_Email_Error}    /html/body/div[2]/div/div[2]/section/form/div[3]/span[2]
${Hospital_Address_Error}    /html/body/div[2]/div/div[2]/section/form/div[4]/span[2]
${Hospital_Pincode_Error}    /html/body/div[2]/div/div[2]/section/form/div[5]/span[2]
${Delete_Icon}    //*[@id="app"]/div/div/div/div/section/div/div/div[1]/table/tbody/tr[1]/td[7]/i
${Hospital_could_not_be_added}    /html/body/div[3]/div/div/span
${Hosital_could_not_added}       Hospital could not be added
${Hosital_succesfully_added}    Successfully added the hospital
${Snack_bar}    /html/body/div[2]/div/div/span
${email_error}   //*[@id='app']/div/div/div/div[1]/form/div[1]/span[3]
${password_error}   //*[@id="app"]/div/div/div/div[1]/form/div[2]/span[3]
${auto_logout}      //*[@id="app"]/div/div/div[2]/div[1]/div
${auto_logout_OK}    /html/body/div[2]/div/div[2]/nav/button
${auto_logout_text}      Your session has been expired! Click on the button to login!
${Login_button_auto_logout}    //*[@id="app"]/div/div/div[2]/div[2]/button
${VALID_USER_stage}     admin@wbcstage97.webcardio.in
${VALID_PASSWORD_stage}    V2+Ki4fa/rxIJuhuHwHzYg==
${LOGIN URL_stage}      https://wbcstage97.webcardio.in/admin
#${LOGIN URL_stage}      http://${SERVER}
${WELCOME URL1}    http://${SERVER}
${ERROR URL1}      http://${SERVER}/error.html
#global    {CONFIG1}
#${CONFIG1}
#${BROWSER}
*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    #LOG TO CONSOLE    ${CONFIG1}
    #Set Selenium Speed    ${DELAY}
    #Click Element    id=input_0
    #Go To    ${LOGIN URL}
    #Login Page Should Be Open

Input Email
    [Arguments]    ${username}
    Wait Until Page Contains Element    xpath=${LOGIN}
    Input Text    xpath=${LOGIN}    ${username}

Input Password
    [Arguments]    ${password}
    Wait Until Page Contains Element    xpath=(${PASSWORD1})
    Input Text    xpath=(${PASSWORD1})    ${password}

Submit Credentials
    click element    xpath=${LOGIN_BTN}

Welcome Page Should Be Open
    Wait Until Page Contains Element    xpath=(//button[@type='button'])[24]    timeout=3
    
Open Browser To Login Page by https
    Open Browser    ${LOGIN URL_stage}    ${BROWSER}
    Maximize Browser Window
    #Set Selenium Speed    ${DELAY}
    #Click Element    id=input_0
    #Go To    ${LOGIN URL}
    #Login Page Should Be Ope

Open Browser To Login Page by firefox
    Open Browser    ${LOGIN URL}    ${browserType}
    Maximize Browser Window

logout

    click element    xpath=${Logout_Btn}


###################################################################
# KeyWord Name : SUITE READINESS
# Usage        : For Testrail credentials
#
###################################################################
SUITE READINESS
     #LOG TO CONSOLE    ${CONFIG1}
     #${CONFIG1}    '7.0, Chrome'
     #LOG TO CONSOLE    ${BROWSER}
     ${project_id} =    get_projectid    ${ProjectName}    ${TestRailUrl}    ${TestRailUser}    ${TestRailPassword}
	 ${plan_id} =    get_planid    ${PlanName}    ${project_id}    ${TestRailUrl}    ${TestRailUser}    ${TestRailPassword}
	 ${run_id} =    get_runid    ${plan_id}    ${TestRailUrl}    ${TestRailUser}    ${TestRailPassword}    ${CONFIG1}
	 #${run_id}    34
	 Set Global Variable      ${run_id}
	 Set Suite Variable       ${SuccessMessage}    "TEST RESULTS"
	 Set Suite Variable       ${ExecutionMessage}    "TEST RESULTS"
     LOG TO CONSOLE    ${run_id}
     #LOG TO CONSOLE    ${CONFIG1}

###################################################################
# KeyWord Name : TEST RESULT UPDATE AND MODE CHANGE
# Usage        : Updating the Test Result in Testrail
#
###################################################################
TEST RESULT UPDATE AND MODE CHANGE
      [Arguments]    ${runid}    ${caseid}    ${status}    ${comment}
      RUN KEYWORD AND IGNORE ERROR    TEST RESULT UPDATE    ${runid}    ${caseid}    ${status}    ${comment}
      #TEST RESULT UPDATE    ${runid}    ${caseid}    ${status}    ${comment}
      #update_result    ${runid}    ${caseid}    ${status}    ${comment}    ${TestRailUrl}    ${TestRailUser}    ${TestRailPassword}
      #log to console    ${runid}
###################################################################
# KeyWord Name : TEST RESULT UPDATED
# Usage        : Updating the Test Result in Testrail
#
###################################################################
TEST RESULT UPDATED
      [Arguments]    ${runid}    ${caseid}    ${status}    ${comment}
      RUN KEYWORD AND IGNORE ERROR    TEST RESULT UPDATE    ${runid}    ${caseid}    ${status}    ${comment}

###################################################################
# KeyWord Name : SUITE READINESS
# Usage       TEST RESULT UPDATE
# Usage        : Updating Test Result in Testrail
#
###################################################################
TEST RESULT UPDATE
    [Arguments]    ${runid}    ${caseid}    ${status}    ${comment}
	update_result    ${runid}    ${caseid}    ${status}    ${comment}    ${TestRailUrl}    ${TestRailUser}    ${TestRailPassword}
	LOG TO CONSOLE   ${comment}
	LOG TO CONSOLE   ${runid}
	LOG TO CONSOLE   ${caseid}
    LOG TO CONSOLE   ${status}
###################################################################

LOG STEP RESULTS
	[Arguments]    ${SucessLog}
	Set Test Message    ${SucessLog}    append=yes
	Set Test Message    ${\n}    append=yes