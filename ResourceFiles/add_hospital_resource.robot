*** Settings ***

Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
#Library           RequestsLibrary
Library           Collections
#Library           String
Library            FakerLibrary    locale=en_US
#Library           ../ConfigurationBase/fakename.py
Resource          resource.robot
Resource          login_resources.robot
Variables          ../ConfigurationBase/patch.py
#Resource          ../TestCases/add_hospital.txt

***Variables***
#${ind} =    set variable    ${1}
#${ind1} =    set variable    ${1}
#${pincode_temp}    Random Digit Random Digit Random Digit Random Digit Random Digit Random Digit

***Keywords***


add_button
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}

adding_valid_details
    #Maximize Browser Window
    #Valid Login
    #wait until element is visible  xpath=${ADD_BTN}
    #click element  ${ADD_BTN}
    ${hosp_name}    User Name
    wait until element is visible  xpath=${Hospital_Name}
    Input Text    xpath=${Hospital_Name}    ${hosp_name}
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile}
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    ${hosp_name}@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    Care,India
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    ${pin}
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    #Scroll Element into View    xpath=${Hospital_Phone}
    #sleep  5s
    click element    xpath=${ADD_Hospital_BTN}
    #wait until element is visible  xpath=$
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    #click element    xpath=${Cancel_Hospital_BTN}
    #sleep  2s
    #click element    ${Logout_Btn}
    #sleep    2s
    #[Teardown]    Close Browser


adding_invalid_details
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    #Input Text    xpath=${Hospital_Name}    null
    #Clear Element Text    xpath=${Hospital_Name}
    sleep   2s
    Press Key    xpath=${Hospital_Name}    \\09
    Page should contain    Name is Required
    #Page Should Contain Element    xpath=${Hospital_Name_Error}
    wait until element is visible  xpath=${Hospital_Phone}
    sleep   2s
    Press Key    xpath=${Hospital_Phone}    \\09
    #Input Text    xpath=${Hospital_Phone}    1234
    #Page Should Contain Element    xpath=${Hospital_Phone_Error}
    sleep   2s
    Page should contain   please enter valid phone number
    wait until element is visible  xpath=${Hospital_Email}
    sleep   2s
    Press Key    xpath=${Hospital_Email}    \\09
    #Input Text    xpath=${Hospital_Email}    kims@webcardio
    #Page Should Contain Element    xpath=${Hospital_Email_Error}
    Page should contain   Please enter a valid email id
    wait until element is visible  xpath=${Hospital_Address}
    sleep   2s
    Press Key    xpath=${Hospital_Address}    \\09
    #Input Text    xpath=${Hospital_Address}
    #Page Should Contain Element    xpath=${Hospital_Address_Error}
    Page should contain    Address is Required
    wait until element is visible  xpath=${Hospital_Pincode}
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    #Input Text    xpath=${Hospital_Pincode}    6868
    #Page Should Contain Element    xpath=${Hospital_Pincode_Error}
    Page should contain    Please enter a valid pin code
    sleep    2s
    #Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${Cancel_Hospital_BTN}
    #click element    xpath=${ADD_Hospital_BTN}
    sleep  5s
    #[Teardown]    Close Browser

adding_existing_hospital_name
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    Input Text    xpath=${Hospital_Name}        care
    #${ind} =    Evaluate    ${ind}+${ind1}
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile1}
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    care1@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    gedgeon1
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    ${pin1}
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  3s
    page should contain element    xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    #click element    xpath=${Cancel_Hospital_BTN}
    sleep    2s

adding_existing_hospital_phone
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    ${hosp_name}    User Name
    Input Text    xpath=${Hospital_Name}   wegh
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    9400095273
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    care2@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    hosp_name@webcardio.in
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    ${pin2}
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  3s
    page should contain element    xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    #click element    ${Logout_Btn}
    #click element    xpath=${Cancel_Hospital_BTN}
    sleep    2s
    #Valid Login

adding_existing_hospital_email
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    ${hosp_name}    User Name
    Input Text    xpath=${Hospital_Name}    asadsad
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile2}
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    care@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    gedgeon123
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    ${pin3}
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  3s
    page should contain element    xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    #click element    xpath=${Cancel_Hospital_BTN}
    sleep    2s

adding_existing_hospital_pincode
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    ${hosp_name}    User Name
    Input Text    xpath=${Hospital_Name}    ${hosp_name}
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile3}
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    care3@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    gedgeon12345
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    682021
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  3s
    page should contain element    xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    #click element    xpath=${Cancel_Hospital_BTN}
    sleep    2s

adding_existing_hospital_name_and_pincode
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    Input Text    xpath=${Hospital_Name}    care
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile4}
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    care4@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    care,kochi
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    682021
    sleep   2s
    Press Key    xpath=${Hospital_Pincode}    \\09
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  3s
    page should contain element    xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    Hospital Aready exist
    #click element    xpath=${Cancel_Hospital_BTN}
    sleep    2s
    click element    xpath=${Logout_Btn}
    Valid Login



cancel_button
    wait until element is visible  xpath=${ADD_BTN}
    click element  ${ADD_BTN}
    ${hosp_name}    User Name
    wait until element is visible  xpath=${Hospital_Name}
    Input Text    xpath=${Hospital_Name}    ${hosp_name}\t
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    ${mobile}\t
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    ${hosp_name}@webcardio.in\t
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    Care,India\t
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    ${pin}\t
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    #Scroll Element into View    xpath=${Hospital_Phone}
    #sleep  5s
    #click element    xpath=${ADD_Hospital_BTN}
    #wait until element is visible  xpath=$
    #wait until element is visible   xpath=${Snack_Bar}
    #Element Should Contain    xpath=${Snack_Bar}    ${Hosital_succesfully_added}
    click element    xpath=${Cancel_Hospital_BTN}
    #sleep  2s
    #click element    ${Logout_Btn}
    sleep    2s
    [Teardown]    Close Browser



