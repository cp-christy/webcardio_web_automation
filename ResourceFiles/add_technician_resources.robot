*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           String
Library           FakerLibrary    locale=en_US
#Library           ../ConfigurationBase/fakename.py
Resource          resource.robot
Resource          login_resources.robot
Variables          ../ConfigurationBase/patch.py
#Resource          ../TestCases/add_hospital.txt
Library  ImapLibrary

*** Variables ***

${add_technician_btn}  //*[@id="app"]/div/div/div/div/section/div/div/div/button
${tech_name}    //*[@id="name"]
${tech_email}    //*[@id="email"]
${tech_staff_id}    //*[@id="staff"]
${tech_add_btn_popup}     //*[@id="add"]
${tech_cancel_btn_popup}    //*[@id="cancel"]
${invalid_mail_id_toast}    /html/body/div[2]/div/div[2]/section/form/div[2]/span[2]
${email_id}    //*[@id="identifierId"]
${email_password}    //*[@id="password"]/div[1]/div/div[1]/input
${email_next_btn}    //*[@id="identifierNext"]/content/span
${password_next_btn}    //*[@id="passwordNext"]/content
${set_password}    //*[@id="password"]
${confirm_password}    //*[@id="confirmPassword"]
${set_btn}    //*[@id="set"]
${name_to_check}    a
*** Keywords ***
Select Technician Tab
    sleep  2s
    click element  ${Tech_Tab}

Add Technician
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    ${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    ${tech_name1}
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    ${tech_name1}@webcardio.in
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    sleep   2s
    Press Key    xpath=${tech_staff_id}    \\09
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    Successfully created the technician
    sleep  5s

Invalid Technician Mail Id
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    #${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    sleep   2s
    Press Key    xpath=${tech_name}    \\09
    #Input Text    xpath=${tech_name}    ${tech_name1}
    wait until element is visible  xpath=${tech_email}
    sleep   2s
    Press Key    xpath=${tech_email}    \\09
    #Input Text    xpath=${tech_email}    ${tech_name1}@webcardio
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    sleep   2s
    Press Key    xpath=${tech_staff_id}    \\09
    page should contain   Please enter a valid email id
    click element  ${tech_cancel_btn_popup}

Name Verification
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    ${tech_name1}     User Name
    wait until element is visible  xpath=${tech_name}
    #Input Text    xpath=${tech_name}
    sleep   2s
    Press Key    xpath=${tech_name}    \\09
    page should contain  Name is required
    sleep   2s
    Input Text    xpath=${tech_name}  abcdefghijklmnopqrstuvwxyz1234567890abcde
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    ${tech_name1}@webcardio.in
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    Successfully created the technician
    sleep  5s

Existing Mail Id
    click element  ${add_technician_btn}
    ${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    ${tech_name1}
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    asd@asd.com
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    User already exists
    sleep  5s

Sending Activation Link
    sleep    5s
    click element  ${add_technician_btn}
    ${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    TechnicianBond
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    webcardio.test2+${tech_name1}@gmail.com
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    Successfully created the technician
    sleep  5s
    Open Mailbox    server=imap.googlemail.com    user=webcardio.test2@gmail.com    password=webcardio@1234    timeout=60
    #Open Browser To Email
    #wait until element is visible  ${email_id}
    #input text  ${email_id}    webcardio.test2
    #click element  ${email_next_btn}
    #wait until element is visible  ${email_password}
    #input text  ${email_password}    webcardio@1234
    #click element  ${password_next_btn}
    ${LATEST}= 	Wait for Mail 	fromEmail=noreply@webcardio.in    status=UNSEEN 	timeout=150
    ${numb}=    Convert To Integer    4
    #${HTML}= 	Open Link from Mail 	${LATEST}    ${numb}
    ${HTML}=    Get Email body    ${LATEST}
    ${Sub}=     Get Substring     ${HTML}    97    300
    ${Sub}=     Get Line    ${HTML}    ${numb}
    #log to console  ${LATEST}
    #sleep  5s
    ${Sub1}=    get substring  ${Sub}    23    -46
    open browser  ${Sub1}    ${BROWSER}

Activate Technician

    #log to console  ${Sub1}

    #${TEXT_LINK}=    fetch from left  ${HTML}    04173037851 to setup your WebCardio Tech account password
    #log to console  ${TEXT_LINK}
    wait until element is visible  ${set_password}
    input text  ${set_password}    12345678
    input text  ${confirm_password}    12345678
    click element  ${set_btn}
    sleep  5s
    page should contain  Congratualations! Password Has Been Set Successfully

Confirm Technician
    Open Mailbox    server=imap.googlemail.com    user=webcardio.test2@gmail.com    password=webcardio@1234    timeout=60
    ${LATEST}= 	Wait for Mail 	fromEmail=noreply@webcardio.in    status=UNSEEN 	subject=Welcome TechnicianBond    {timeout=150
    close browser

Same Technician
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    basil
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    basil@webcardio.in
    wait until element is visible  xpath=${tech_staff_id}
    Input Text    xpath=${tech_staff_id}    WBC175
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    User already exists
    sleep  5s

Same Name
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    ${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    Basil
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    ${tech_name1}@webcardio.in
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    Successfully created the technician
    sleep  5s

Assistant Email Id
    wait until element is visible  xpath=${add_technician_btn}
    click element  ${add_technician_btn}
    ${tech_name1}    User Name
    wait until element is visible  xpath=${tech_name}
    Input Text    xpath=${tech_name}    ${tech_name1}
    wait until element is visible  xpath=${tech_email}
    Input Text    xpath=${tech_email}    allisongalloway@webcardio.in
    wait until element is visible  xpath=${tech_staff_id}
    ${tech_id}    Password
    Input Text    xpath=${tech_staff_id}    ${tech_id}
    click element  ${tech_add_btn_popup}
    wait until element is visible   xpath=${Snack_Bar}
    Element Should Contain    xpath=${Snack_Bar}    User already exists
    sleep  5s




