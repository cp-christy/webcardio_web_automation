*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

***Keywords***

Invalid Login
    #Open Browser To Login Page
    Input Email    ${VALID_USER}    #invalid password
    Input Password    RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwM
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3
    Input Email  basil@wbcdev01.webcardio.in   #invalid username
    Input Password    ${VALID_PASSWORD}
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3
    Input Email  Basil@wbcdev01.webcardio.in   #invalid username
    Input Password  RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwM
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3

Password less than 8 character
    Input Email  Basil@wbdev01.webcard                                 #invalid username
    #page should contain element    xpath=${email_error}    timeout=3
    sleep   2s
    Input Password  RMe       #invalid password
    Sleep   1s
    Submit Credentials
    sleep   2s
    Element should contain    xpath=//*[@id="app"]/div/div/div[1]/div[1]/form/div[1]/span[2]    Please enter a valid email id
    #page should contain element    xpath=//*[@id="app"]/div/div/div/div[1]/form/div[2]/span[3]    timeout=3
    sleep   2s
    Element should contain    xpath=//*[@id="app"]/div/div/div[1]/div[1]/form/div[2]/span[2]    Password must be atleast 8 characters
    sleep   2s
    #[Teardown]    Close Browser

Valid Login
    #Open Browser To Login Page
    Input Email    ${VALID_USER}
    Input Password    ${VALID_PASSWORD}
    sleep  2s
    Submit Credentials
    sleep  4s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    #[Teardown]    Close Browser

Valid_Login_refresh
    #Open Browser To Login Page using Chrome https
    #Input Email    ${VALID_USER_stage}
    #Input Password    ${VALID_PASSWORD_stage}
    #sleep  2s
    #Submit Credentials
    sleep  2s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    Sleep   1s
    Reload Page
    Sleep   2s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    #[Teardown]    Close Browser

Landing_page
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    timeout=3
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}


Valid_Login_and_go_back

    #Open Browser To Login Page using Chrome
    #Input Email    ${VALID_USER}
    #Input Password    ${VALID_PASSWORD}
    #sleep  2s
    #Submit Credentials
    sleep  2s
    #page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}   timeout=3
    #page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    #Sleep   4s
    #Reload Page
    #Sleep   3s
    #page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    #page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    Go Back
    sleep   5s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    timeout=3
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    #Sleep   4s


Auto_log_out
    #Open Browser To Login Page using Chrome
    #Input Email    ${VALID_USER}
    #Input Password    ${VALID_PASSWORD}
    #sleep  2s
    #Submit Credentials
    #sleep  3s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    Sleep  600s
    Page should contain    Your session has been expired!
    Click element    xpath=${auto_logout_OK}
    #Element should contain    xpath=/html/body/div[2]/div/div[2]/section/p/strong    Your session has been expired!
    #Click element    /html/body/div[2]/div/div[2]/nav/button
    #Close Browser

Login_without_internet
     ${T_value}=  wifioff
     sleep   120s
     Input Email    ${VALID_USER}
     Input Password    ${VALID_PASSWORD}
     sleep  2s
     Submit Credentials
     Element should contain    ${LOGIN_FAILED}    No network connection
     ${T_value}=  wifion
     sleep    120s


Valid Login https
    #Open Browser To Login Page
    Input Email    ${VALID_USER_stage}
    Input Password    ${VALID_PASSWORD_stage}
    sleep  2s
    Submit Credentials
    sleep  3s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    Sleep   4s
    #[Teardown]    Close Brows

Close
    LOG TO CONSOLE   ${TEST STATUS}

