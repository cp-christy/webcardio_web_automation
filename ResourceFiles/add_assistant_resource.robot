*** Settings ***

Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           String
Library           FakerLibrary    locale=en_US
#Library           ../ConfigurationBase/fakename.py
Resource          resource.robot
Resource          login_resources.robot
Variables          ../ConfigurationBase/patch.py
#Variables         resource.robot
#Variables         login_resources.robot
#Resource          ../TestCases/add_hospital.txt

***Variables ***

${add_assist_btn}    //*[@id="app"]/div/div/div/div/section/div/div/button
${assist_name}    //*[@id="name"]
${assist_email}    //*[@id="email"]
${assist_id}    //*[@id="staff"]
${as_add_btn}    //*[@id="add"]
${as_cancel_btn}    //*[@id="cancel"]
${as_snack_bar}    /html/body/div[2]/div/div
${as_name_error}    /html/body/div[2]/div/div[2]/section/form/div[1]/span[2]
${as_email_error}    /html/body/div[2]/div/div[2]/section/form/div[2]/span[2]
${as_id_error}    /html/body/div[2]/div/div[2]/section/form/div[3]/span[2]

*** Keywords ***

Add Assistant
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Valid Login
    click element  xpath=${Asst_Tab}
    click element  xpath=${add_assist_btn}
    ${as_name}    User Name
    page should contain element    xpath=${add_assist_btn}
    wait until element is visible  xpath=${assist_name}
    Input Text    xpath=${assist_name}    ${as_name}
    wait until element is visible  xpath=${assist_email}
    Input Text    xpath=${assist_email}    ${as_name}@webcardio.in
    ${as_id}    Password
    wait until element is visible  xpath=${assist_id}
    Input Text    xpath=${assist_id}    ${as_id}
    sleep   2s
    click element  ${as_add_btn}
    sleep  5s
    element should contain    xpath=${as_snack_bar}    Successfully added the assistant

Invalid Assistant
    click element  xpath=${add_assist_btn}
    #page should contain element    xpath=${add_assist_btn}
    wait until element is visible  xpath=${assist_name}
    Input Text    xpath=${assist_name}    ba
    sleep   2s
    page should contain element  xpath=${as_name_error}   # Name must be atleast 3 characters
    wait until element is visible  xpath=${assist_email}
    Input Text    xpath=${assist_email}    ba@webcardio
    sleep   2s
    element should contain    xpath=${as_email_error}    Please enter a valid email id
    wait until element is visible  xpath=${assist_id}
    Input Text    xpath=${assist_id}    ba
    sleep   2s
    element should contain    xpath=${as_id_error}    Staff id must be atleast 3 characters
    sleep   2s
    click element  ${as_cancel_btn}