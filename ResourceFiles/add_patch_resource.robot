*** Settings ***

Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           String
Library           FakerLibrary    locale=en_US
#Library           ../ConfigurationBase/fakename.py
Resource          resource.robot
Resource          login_resources.robot
Variables          ../ConfigurationBase/patch.py
#Variables         resource.robot
#Variables         login_resources.robot
#Resource          ../TestCases/add_hospital.txt

***Variables ***
${add_patch_btn}   //*[@id="app"]/div/div/div/div/section/div/div/button
${add_patch_text}   //*[@id="patches"]
${add_patch}    //*[@id="add"]
${cancel_patch_btn}   //*[@id="cancel"]
${first_hospital_list}    //*[@id="app"]/div/div/div/div/section/div/div[1]/ul/li[1]
${gardin}    //*[@id="app"]/div/div/div/div/section/div/div[1]/ul/li[4]
${patch_added_notification}    /html/body/div[2]/div/div[2]/section/table/tbody/tr
${ok_btn}    /html/body/div[3]/div/div[2]/nav/button

${patch_added_success}    /html/body/div[2]/div/div
${window_bar}    /html/body/div[2]/div/div[2]/section
${10th_hospital}         //*[@id="app"]/div/div/div/div/section/div/div[1]/ul/li[10]


*** Keywords ***

Add Patch
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Valid Login
    click element  xpath=${Patches_Tab}
    page should contain element    xpath=${add_patch_btn}
    #page should contain element  xpath=${first_hospital_list}
    #Element Should Contain     xpath=${first_hospital_list}    hospital_1502280632+123456
    #click element  xpath=${first_hospital_list}
    #click element     xpath=${gardin}
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}   ${y}
    click element  xpath=${add_patch}
    sleep   2s
    element should contain    xpath=${patch_added_success}    Successfully assigned all the patches
    sleep   3s
    #click element    xpath=${add_patch_btn}
    #input text  xpath=${add_patch_text}   BASIL
    #click element    ${add_patch}
    #sleep   2s
    #select window   ${window_bar}
    #Element Should Contain    xpath=${patch_added_notification}    User Exists
    #click element    xpath=${ok_btn}
    #click element    xpath=${add_patch_btn}
    #input text  xpath=${add_patch_text}   BASILA
    #click element    xpath=${add_patch}
    #sleep    5s
    #page should contain element  xpath=${patch_added_notification}
    #Element Should Contain    xpath=${patch_added_notification}    MUST BE FIVE CHARACTER
    #click element  ${ok_btn}

Add Patch In Bulk
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}   ${y1}\n${y2}\n${y3}\n${y4}\n${y5}
    click element    xpath=${add_patch}
    sleep   2s
    element should contain    xpath=${patch_added_success}    Successfully assigned all the patches
    sleep   3s
Case Sensitive
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}    ${y6}
    click element    xpath=${add_patch}
    sleep   2s
    element should contain    xpath=${patch_added_success}    Successfully assigned all the patches
    sleep   3s
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}    ${y7}
    click element    xpath=${add_patch}
    sleep   2s
    element should contain    xpath=${patch_added_success}    Successfully assigned all the patches
    sleep   3s
Special Character
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}    ${y8}
    click element    xpath=${add_patch}
    sleep   2s
    element should contain    xpath=${patch_added_success}    Successfully assigned all the patches
    sleep   3s
Existing Patch
    click element    xpath=${add_patch_btn}
    input text  xpath=${add_patch_text}   Basil
    click element    xpath=${add_patch}
    #page should contain element  id=_2J-aP90fppafQgKbNyQD5V
    sleep   2s
    page should contain    xpath=${ok_btn}
Select hospital
    Table Should Contain  xpath=${first_hospital_list}
    click element    xpath=${first_hospital_list}



