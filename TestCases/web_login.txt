*** Settings ***
Library           Selenium2Library    10
Resource          ../ResourceFiles/resource.robot
Resource          ../ResourceFiles/login_resources.robot
Suite Setup        SUITE READINESS
Suite Teardown    Close Browser

*** Test Cases ***

################################################################################
# Test Cases ID :- C629 [INVALID USERNAME AND PASSWORD]
##############################################################################

Test case 1: Invalid Login
       [Documentation]    Invalid_Login [C629]
       [Tags]        WBC-9
       Open Browser To Login Page
       #Wait Until Keyword Succeeds  30s  2s  TEST STATUS
	   #Invalid Login
	   LOG STEP RESULTS    Passed
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    629    ${TEST STATUS}    ${TEST MESSAGE}


################################################################################
# Test Cases ID :- C630 [PASSWORD LESS THAN 8]
##############################################################################

Test case 2: Password less than 8 character
       [Documentation]    Password less than 8[C630]
       [Tags]        WBC-9
       Password less than 8 character
       LOG STEP RESULTS    Password less than 8[C630]
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    630    ${TEST STATUS}    ${TEST MESSAGE}


################################################################################
# Test Cases ID :- C628 [VALID USERNAME AND PASSWORD]
##############################################################################

Test case 3: Valid Login
	   [Documentation]    valid_Login[C6]
       [Tags]        WBC-9
       #Open Browser To Login Page
	   Valid Login
	   LOG STEP RESULTS    valid_Login[C628]
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    628    ${TEST STATUS}    ${TEST MESSAGE}




################################################################################
# Test Cases ID :- C683 [LANDING PAGE AFTER LOGIN]
##############################################################################
Test case 4: Landing Page
	   [Documentation]    Landing Page[C683]
       [Tags]        WBC-9
       #Open Browser To Login Page
	   Landing_page
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    683    ${TEST STATUS}    ${TEST MESSAGE}


################################################################################
# Test Cases ID :- C636 [PAGE REFRESH AFTER LOGIN]
##############################################################################

Test case 5: Refreshing the page after login
	   [Documentation]     Page refresh[C636]
       [Tags]        WBC-9
	   Valid_Login_refresh
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    636    ${TEST STATUS}    ${TEST MESSAGE}


################################################################################
# Test Cases ID :- C754 [BACK BUTTON AFTER LOGIN]
##############################################################################

Test case 6: Clicking Back button after login
	   [Documentation]    Back button after login[C754]
       [Tags]        WBC-9
	   Valid_Login_and_go_back
	   [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    754    ${TEST STATUS}    ${TEST MESSAGE}


################################################################################
# Test Cases ID :- C637 [LOGOUT]
##############################################################################

Test case 7: Logout
	   [Documentation]     Log Out[C637]
       [Tags]        WBC-9
	   logout
       [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    637    ${TEST STATUS}    ${TEST MESSAGE}
################################################################################
# Test Cases ID :- C755 [AUTO LOGOUT]
##############################################################################

Test case 8: Verify auto logout
	   [Documentation]        Auto Logout [C755]
       [Tags]        WBC-9
       Valid login
	   Auto_log_out
       [Teardown]    TEST RESULT UPDATE AND MODE CHANGE    ${runid}    755    ${TEST STATUS}    ${TEST MESSAGE}



















################################################################################
# Test Cases ID :- C775 [EMPTY EMAIL AND PASSWORD]
##############################################################################

#Test case 6: Invalid Login using https
       #[Documentation]     Invalid_Login by https
       #[Tags]        WBC-9
       #Open Browser To Login Page by https
	   #Invalid Login
################################################################################
# Test Cases ID :- C775 [EMPTY EMAIL AND PASSWORD]
##############################################################################

#Test case 7: Valid Login using https
	   #[Documentation]        valid_Login by https
       #[Tags]        WBC-9
       #Open Browser To Login Page
	   #Valid Login https
################################################################################
# Test Cases ID :- C775 [EMPTY EMAIL AND PASSWORD]
##############################################################################


#Test case 8: Refreshing the page after login in https
	   #[Documentation]        Page refresh
       #[Tags]        WBC-9
	   #Valid_Login_refresh

################################################################################
# Test Cases ID :- C775 [EMPTY EMAIL AND PASSWORD]
##############################################################################

#Test case 9: Clicking Back button after login in https
	   #[Documentation]        Back
       #[Tags]        WBC-9
	   #Valid_Login_and_go_back


#Test case 9: Login without internet by https
	   #[Documentation]        Back
       #[Tags]        WBC-9
	   #Login_without_internet


#Test case 10: Verify auto logout in https
	   #[Documentation]        Back
       #[Tags]        WBC-9
       #logout
	   #Auto_log_out

