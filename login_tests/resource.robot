*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}         wbcdev01.webcardio.in/admin/
${BROWSER}        Chrome
${DELAY}          10
${VALID_USER}     admin@wbcdev01.webcardio.in
${VALID_PASSWORD}    RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwMykSzDiq3PJCFXW
${LOGIN URL}      http://${SERVER}
${WELCOME URL}    http://${SERVER}
${ERROR URL}      http://${SERVER}/error.html
${LOGIN}    //*[@id="app"]/div/div/div/div[1]/form/div[1]/input
${PASSWORD1}    //*[@id="app"]/div/div/div/div[1]/form/div[2]/input
${LOGIN_BTN}    //*[@id="app"]/div/div/div/div[2]
${ADD_TAB}    //*[@id="app"]/div/div/div/div/div/nav/div[1]
${LOGIN_FAILED}  /html/body/div[2]/div
${ADD_BTN}  //*[@id="app"]/div/div/div/div/section/div/div[1]/button
${Hospital_Name}  /html/body/div[2]/div/div[2]/section/form/div[1]/input
${Hospital_Phone}  /html/body/div[2]/div/div[2]/section/form/div[2]/input
${Hospital_Email}  /html/body/div[2]/div/div[2]/section/form/div[3]/input
${Hospital_Address}  /html/body/div[2]/div/div[2]/section/form/div[4]/textarea
${Hospital_Pincode}  /html/body/div[2]/div/div[2]/section/form/div[5]/input
${Add_Hospital_BTN}  /html/body/div[2]/div/div[2]/section/form/button[1]
${Cancel_Hospital_BTN}  /html/body/div[2]/div/div[2]/section/form/button[2]
${Hospital_Tab}   //*[@id="app"]/div/div/header/div/nav/a[1]/a
${Patches_Tab}  //*[@id="app"]/div/div/header/div/nav/a[2]/a
${Doctors_Tab}  //*[@id="app"]/div/div/header/div/nav/a[3]/a
${Asst_Tab}  //*[@id="app"]/div/div/header/div/nav/a[4]/a
${Tech_Tab}     //*[@id="app"]/div/div/header/div/nav/a[5]/a
${Logout_Btn}   //*[@id="app"]/div/div/header/div/button
*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    #Set Selenium Speed    ${DELAY}
    #Click Element    id=input_0
    #Go To    ${LOGIN URL}
    #Login Page Should Be Open

Input Email
    [Arguments]    ${username}
    Wait Until Page Contains Element    xpath=${LOGIN}
    Input Text    xpath=${LOGIN}    ${username}

Input Password
    [Arguments]    ${password}
    Wait Until Page Contains Element    xpath=(${PASSWORD1})
    Input Text    xpath=(${PASSWORD1})    ${password}

Submit Credentials
    click element    xpath=${LOGIN_BTN}

Welcome Page Should Be Open
    Wait Until Page Contains Element    xpath=(//button[@type='button'])[24]    timeout=20