*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot
Library           fakename.py

*** Test Cases ***
Create Patient
    Open Browser To Login Page
    Input Username    admin@webcardio.in
    Input Password    geon1234
    Submit Credentials
    Welcome Page Should Be Open
    Add Patient
    #Enter the Patient details
    #Click Submit
    Sleep    4s
    [Teardown]    Close Browser
