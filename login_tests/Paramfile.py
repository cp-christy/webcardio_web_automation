# =====================================================================================
#    Filename   :  RFW_ParamFile.py
#
#    Description:  This para file contains all the user defined values used for 
#               :  creating Webcardio Patient.
#
#    Version    :  1.0
#    Created    :  
#    Compiler   :  python
#    Author     :     
#    Company    :  Gadgeon, Kochi
#
#    Revision History:
#
# =====================================================================================



#######################################################################################
# Array Name : WD_DETAILS
# Usage      : Alertpoint and Gateway's Credentials is stored in this Array
#
# Value1     : PanicId 
# Value2     : PanicColor
# Value3     : UserName (in gateway log)
# Value4     : Location (in gateway log)
# Value5     : Username 
# Value6     : Password 
# Value7     : Web GUI IP
# Value8     : Web GUI Username
# Value9     : Web GUI Password to acces GUI
########################################################################################


WD_DETAILS = [["basil","2","qwer@webcardio.in","8129515708","password","12","12"]]

########################################################################################
