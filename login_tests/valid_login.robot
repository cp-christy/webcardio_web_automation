*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***

Invalid Login
    Open Browser To Login Page
    Input Email    ${VALID_USER}    #invalid password
    Input Password    RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwM
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3
    Input Email  basil@wbcdev01.webcardio.in   #invalid username
    Input Password    ${VALID_PASSWORD}
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3
    Input Email  Basil@wbcdev01.webcardio.in   #invalid username
    Input Password  RMeHANuSOkKAPpGJBS7d8MHbyrIA289wgsOAaA8x90MLc8WGwM
    sleep  2s
    Submit Credentials
    sleep  3s
    page should not contain element    ${ADD_BTN}    timeout=3
    #[Teardown]    Close Browser

Valid Login
    #Open Browser To Login Page
    Input Email    ${VALID_USER}
    Input Password    ${VALID_PASSWORD}
    sleep  2s
    Submit Credentials
    sleep  3s
    page should contain element    ${Hospital_Tab}    ${Patches_Tab}    ${Tech_Tab}
    page should contain element    ${Doctors_Tab}    ${Asst_Tab}    ${Logout_Btn}
    #[Teardown]    Close Browser

Add Hospital
    click element  ${ADD_BTN}
    wait until element is visible  xpath=${Hospital_Name}
    Input Text    xpath=${Hospital_Name}    kims
    wait until element is visible  xpath=${Hospital_Phone}
    Input Text    xpath=${Hospital_Phone}    1234567890
    wait until element is visible  xpath=${Hospital_Email}
    Input Text    xpath=${Hospital_Email}    kims@webcardio.in
    wait until element is visible  xpath=${Hospital_Address}
    Input Text    xpath=${Hospital_Address}    Webcardio inc PO Kakkanad
    wait until element is visible  xpath=${Hospital_Pincode}
    Input Text    xpath=${Hospital_Pincode}    682021
    wait until element is visible  xpath=${ADD_HOSPITAL_BTN}
    click element    xpath=${ADD_Hospital_BTN}
    sleep  2s
    click element  xpath=${Cancel_Hospital_BTN}
    sleep  2s
    click button    ${Logout_Btn}
    sleep  2s
    page should contain element    xpath=${LOGIN}
    sleep  2s
    [Teardown]    Close Browser
