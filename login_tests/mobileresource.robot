*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
#Library           Selenium2Library
#Library           fakename.py
Library           AppiumLibrary    run_on_failure=AppiumLibrary.CapturePageScreenshot





*** Keywords ***
###################################################################
# KeyWord Name : WEBCARDIOAPP
# Usage        : Login via the login page of MOBILE APP
#                check the alert history
###################################################################
LOGINMOBILEAPP
    Set Network Connection Status    1
    Open Application   http://0.0.0.0:4723/wd/hub    platformName=Android    platformVersion=6.0    deviceName=VKZHBIWGSCPFBMGE
    ...    app=${CURDIR}/WBC_JUL1214537.apk    appPackage=com.gadgeon.webcardio    appActivity=com.gadgeon.webcardio.ui.activity.SplashActivity
    AppiumLibrary.Wait Until Page Contains Element    id=com.android.packageinstaller:id/permission_allow_button    timeout=120
    Click Element    id=com.android.packageinstaller:id/permission_allow_button
    AppiumLibrary.Wait Until Page Contains Element    id=com.android.packageinstaller:id/permission_allow_button    timeout=120
    Click Element    id=com.android.packageinstaller:id/permission_allow_button
    AppiumLibrary.Wait Until Page Contains Element    id=com.gadgeon.webcardio:id/email    timeout=120
    AppiumLibrary.Input Text    id=com.gadgeon.webcardio:id/email    Timothy@webcardio.in
    AppiumLibrary.Wait Until Page Contains Element    id=com.gadgeon.webcardio:id/password    timeout=120
    AppiumLibrary.Input Text    id=com.gadgeon.webcardio:id/password    abcd1234
    Click Element    id=com.gadgeon.webcardio:id/loginButton


###################################################################
# KeyWord Name : FINDPATCH
# Usage        : Login via the login page of MOBILE APP
#                check the alert history
###################################################################
FINDPATCH
        :FOR    ${INDEX}    IN    1 20
    \    Log    ${INDEX}
    \    AppiumLibrary.Wait Until Page Contains Element    id=com.gadgeon.webcardio:id/findpatchbutton    timeout=120
    \    Run Keyword If    '${ELEMENT}' == 'Break On Me'    Exit For Loop
    \    Log    Do more actions here ..

    
    


   
    
    #AppiumLibrary.Wait Until Page Contains    Sign In
    #AppiumLibrary.Input Text    id=com.gadgeon.alertpoint:id/signin_email_et    jmarks@barrow.com
    #AppiumLibrary.Input Text    id=com.gadgeon.alertpoint:id/signin_password_et    admin
    #AppiumLibrary.Click Element    id=com.gadgeon.alertpoint:id/toggle_server_t_btn
    #AppiumLibrary.Click Element    id=com.gadgeon.alertpoint:id/signin_btn
    #AppiumLibrary.Wait Until Page Contains Element    id=com.gadgeon.alertpoint:id/action_main_tv    timeout=120
    
